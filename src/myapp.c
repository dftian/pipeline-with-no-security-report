#include <stdio.h>

int basic_division(int numerator, int denominator)
{
    // Do basic division.
    // The issue here is that if denominator is 0,
    // divide-by-0 error will occur.
    return numerator / denominator;

}

int main(int argc, char** argv)
{

    printf("%d", basic_division(5, 0));

    // Print what the user provides a paramter.
    // The issue here is that if the user provides no input, a segfault
    // will occur.
    printf("%s", argv[1]);

}
